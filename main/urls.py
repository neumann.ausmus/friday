from . import views
from django.urls import path

app_name = 'login'
urlpatterns= [
    path('', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('logout/', views.logout, name='logout'),
    path('home/', views.home, name='home')
]