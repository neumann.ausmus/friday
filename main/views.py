from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth
from .models import *


#로그인, 회원가입, 로그아웃
def login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            return redirect('home/')
        else:
            return render(request, 'main/login/login.html', {'error': '아이디 또는 비밀번호가 틀리셨습니다.'})
    else:
        return render(request, 'main/login/login.html')

def signup(request):
    print('회원가입')
    if request.method == "POST":
        if request.POST["password1"] == request.POST["password2"]:
            user = User.objects.create_user(
                username = request.POST["username"],
                password = request.POST["password1"])
            auth.login(request, user)
            return redirect("/")
        return render(request, 'main/login/signup.html')
    return render(request, 'main/login/signup.html')

def logout(request):
    auth.logout(request)
    return redirect('login')

#메인페이지
def home(request):
    return render(request, 'main/home/home.html')